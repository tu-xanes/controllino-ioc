#!../../bin/linux-x86_64/sts

## You may have to change sts to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/sts.dbd"
sts_registerRecordDeviceDriver pdbbase

# test macros
#epicsEnvSet("IOCPREFIX", "XNS:VAC:")
#epicsEnvSet("DEVIP", "192.168.0.214")
#epicsEnvSet("DEVPORT", "23")

## macros
epicsEnvSet("P", "$(IOCPREFIX)")
epicsEnvSet("PORT", "L1")
epicsEnvSet("STREAM_PROTOCOL_PATH", "${TOP}/iocBoot/${IOC}/.")

drvAsynIPPortConfigure("L1","$(DEVIP):$(DEVPORT)",0,0,0)

## Load record instances
dbLoadRecords("${ASYN}/db/asynRecord.db","P=$(P),R=asyn,PORT=$(PORT),ADDR=0,IMAX=256,OMAX=256")

cd "${TOP}/iocBoot/${IOC}"

dbLoadRecords("vac.db","P=$(P), PORT=$(PORT)")
dbLoadRecords("bitlabels.db","P=$(P)inputs:labels1:")
dbLoadRecords("bitlabels.db","P=$(P)inputs:labels2:")
dbLoadRecords("bitlabels.db","P=$(P)outputs:labels1:")
dbLoadRecords("bitlabels.db","P=$(P)outputs:labels2:")
dbLoadRecords("bitlabels.db","P=$(P)extra:labels1:")
dbLoadRecords("bitlabels.db","P=$(P)extra:labels2:")
dbLoadRecords("bitlabels.db","P=$(P)extra:labels3:")

## Autosave Path and Restore settings
set_savefile_path("/EPICS/autosave")
set_requestfile_path("$(TOP)/iocBoot/$(IOC)")
set_pass0_restoreFile("auto_settings.sav")
set_pass1_restoreFile("auto_settings.sav")

iocInit

## Autosave Monitor settings
create_monitor_set("auto_settings.req", 30, "P=$(P)")

## Start any sequence programs
#seq sncxxx,"user=epics"
